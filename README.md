larapus-frontend
================

Design frontend for laravel Perpustakaan http://leanpub.com/seminggubelajarlaravel

# SETUP 

I'm using gulpjs to get livereload. You should to, please check
http://gulpjs.com.


1. Install npm
2. Run `npm install`
3. Run `gulp`
4. Check http://127.0.0.1:4000

Suggestions are welcome.. :)
